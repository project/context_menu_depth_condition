<?php

/**
 * Expose menu items as a context condition.
 */
class context_menu_depth_condition extends context_condition {
  /**
   * Override of condition_values().
   */
  function condition_values() {
    $values = array();
    for ($depth = 1; $depth <= MENU_MAX_DEPTH; $depth++) {
      $values[] = $depth;
    }
    return $values;
  }

  /**
   * {@inheritdoc}.
   */
  function condition_form($context) {
    $options = array();
    foreach ($this->condition_values() as $value) {
      $options[$value] = $value;
    }
    return array(
      '#title' => $this->title,
      '#description' => $this->description,
      '#options' => $options,
      '#type' => 'checkboxes',
      '#default_value' => $this->fetch_from_context($context, 'values'),
    );
  }

  /**
   * Override of execute().
   */
  function execute() {
    $menu_link = menu_link_get_preferred();
    if ($menu_link) {
      foreach ($this->get_contexts($menu_link['depth']) as $context) {
        $this->condition_met($context, $menu_link['depth']);
      }
    }
  }

}
